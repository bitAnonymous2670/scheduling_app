/*

XMLHttpRequest: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest



*/

// ACCOUNT REGISTRATION STUFF
const INVALID_EMAIL_ERROR              = "Invalid Email";
const INVALID_PASSWORD_ERROR           = "Invalid Password";
const MISMATCH_CONF_PASS_ERROR         = "Mismatch Confirmed Password";

const EMPTY_EMAIL_ERROR                = "Empty Email";
const EMPTY_PASSWORD_ERROR             = "Empty Password";
const EMPTY_CONF_PASSWROD_ERROR        = "Empty Confirm Password";

const NULL_EMAIL_MATCH_ERROR           = "Null Email Match";
const NULL_PASSWORD_MATCH_ERROR        = "Null Password Match";
const NULL_CONF_PASSWROD_MATCH_ERROR   = "Bad Confirm Password Match";

const INVALID_EMAIL_MESSAGE            = "The email you have entered is not a valid email address.";
const INVALID_PASSWORD_MESSAGE         = "The password you have entered is not a valid password.";
const INVALID_CONFIRM_PASSWORD_MESSAGE = "The password does not match the confirmation password.";


const EMPTY_EMAIL_MESSAGE              = "You have to enter in an email.";
const EMPTY_PASSWORD_MESSAGE           = "You have to enter in a password.";
const EMPTY_CONFIRM_PASSWORD_ERROR     = "You have to confirm the password you have entered.";


const BASE_URL = "https://192.168.1.202";
const BASE_PORT = "3000/";

var EMAIL_REGEX = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/
var PSSWD_REGEX = /^[A-Za-z0-9]{3,10}$/;
var CFM_REGEX   = PSSWD_REGEX;


var LOGIN_WIDTH       = 400;
var LOGIN_HEIGHT      = 500;

var SCREEN_RES_WIDTH  = (screen.width - LOGIN_WIDTH) / 2;
var SCREEN_RES_HEIGHT = (screen.height - LOGIN_HEIGHT) / 2;


let is_empty = function(str_var, error_list, error_object) {
	if (str_var == "") {
		error_list.push(error_object);
	}

	return error_list;
}

let regex_match = function(str_val, regex_val, error_list, error_object) {
	let var_match = null;


	try {
		var_match = str_val.match(regex_val);
	} catch(e) {
		// I want to send the error to my server so I can catch all the errors or something.
		console.log(e);
		error_object.error = e;
		error_object.message = e.message;
		error_list.push(error_object);
	}

	if (var_match == null) {
		error_list.push(error_object);
	}

	return error_list;
}

let reset_registration_fields = function() {
	$$("register_email").setValue("");
	$$("register_password").setValue("");
	$$("register_confirm_password").setValue("");
}

let display_registration_errors = function(error_list) {
	console.log(error_list);
}


let login_component = {	
	width: LOGIN_WIDTH,
	rows: [
		{ view:"text", label:"Email", id: "login_email"},
		{ view:"text", label:"Password", id:"login_password"},
		{ view: "label" },
		{
			view:"button", value:"Login" , css:"webix_primary",
			on: {
				onItemClick: (id, e) => {
// initialize everything
					let err_obj = null;

					let email_match = null;
					let password_match = null;
					let confirm_match = null;

					let httpRequest = null;

					let errors = [];

// grab contents
					let email = $$("login_email").getValue();
					let psswd = $$("login_password").getValue();

// set empty email error 
					err_obj = {
						 "variable": "email", 
						 "error": EMPTY_EMAIL_ERROR, 
						 "message": EMPTY_EMAIL_MESSAGE
					};
					errors = is_empty(email, errors, err_obj);
					err_obj = null;

// set empty passworkd error
					err_obj = {
						 "variable": "psswd",
						  "error": EMPTY_PASSWORD_ERROR, 
						  "message": EMPTY_PASSWORD_MESSAGE 
					};
					errors = is_empty(psswd, errors, err_obj);
					err_obj = null;


// If there are any errors, display them and leave
					if (errors.length != 0) {
						// This is because Something is empty and needs to be fixed.
						// run error posting
						return display_registration_errors(errors);
					}


// Check to make sure the email passes the regex 
					err_obj = {
						 "variable": "email_match", 
						 "error": NULL_EMAIL_MATCH_ERROR, 
						 "message": INVALID_EMAIL_MESSAGE 
					};
					errors = regex_match(email, EMAIL_REGEX, errors, err_obj);
					err_obj = null;

// Check to make sure the password passes the regex 
					err_obj = {
						 "variable": "password_match", 
						 "error": NULL_PASSWORD_MATCH_ERROR, 
						 "message": INVALID_PASSWORD_MESSAGE 
					};
					errors = regex_match(psswd, PSSWD_REGEX, errors, err_obj);
					err_obj = null;

					if (errors.length != 0) {
						// This is because we have matching errors
						// run error posting
						return display_registration_errors(errors);
					}


					// Old compatibility code, no longer needed.
					if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
					    httpRequest = new XMLHttpRequest();
					} else if (window.ActiveXObject) { // IE 6 and older
					    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
					}

					
					httpRequest.open("POST", BASE_URL + ":" + BASE_PORT + "login");
					httpRequest.setRequestHeader('Content-Type', 'application/json');

					httpRequest.onload = function() {
						if ( httpRequest.status === 200 ) {
							console.log(httpRequest.responseText);
							successful_user_creation(email);
						} else if (httpRequest.status !== 200) {
							console.log(httpRequest.responseText);
						}
					};

					httpRequest.send(JSON.stringify({ "email": email, "password" : psswd }));


				}
			}
		},
		{
			view:"button", value:"Register", 
			on: {
				onItemClick: (id, e) => {
					$$("center_view_multi").setValue("registration_view");
				}
			}
		}
	]
}

let go_to_login = function() {
	$$("center_view_multi").setValue("sign_in_view");
	reset_registration_fields();
}

let successful_user_creation = function(new_email) {

	// display some message like check your email
	go_to_login();
}

let register_component = {
	width: LOGIN_WIDTH,
	rows: [
		{ view:"text", label:"Email", id: "register_email"},
		{ view:"text", label:"Password", id:"register_password"},
		{ view:"text", label:"Confirm", id:"register_confirm_password"},
		{ view: "label" },
		{
			view:"button", value:"Create Account" , css:"webix_primary",
			on: {
				onItemClick: (id, e) => {
					let err_obj = null;

					let email_match = null;
					let password_match = null;
					let confirm_match = null;

					let httpRequest = null;

					let email = $$("register_email").getValue();
					let psswd = $$("register_password").getValue();
					let confirm_psswd = $$("register_confirm_password").getValue();

					let errors = [];


					// Checking For Empty Entry
					err_obj = { "variable": "email", "error": EMPTY_EMAIL_ERROR, "message": EMPTY_EMAIL_MESSAGE };
					errors = is_empty(email, errors, err_obj);
					err_obj = null;

					err_obj = { "variable": "psswd", "error": EMPTY_PASSWORD_ERROR, "message": EMPTY_PASSWORD_MESSAGE };
					errors = is_empty(psswd, errors, err_obj);
					err_obj = null;

					err_obj = { "variable": "confirm_psswd", "error": EMPTY_CONF_PASSWROD_ERROR, "message": EMPTY_CONFIRM_PASSWORD_ERROR };
					errors = is_empty(confirm_psswd, errors, err_obj);
					err_obj = null;

					if (errors.length != 0) {
						// This is because Something is empty and needs to be fixed.
						// run error posting
						return display_registration_errors(errors);
					}


					// make api call for account registration
					// get back response
					// if valid, continue normally
					// if not valid, send back the errors to display


					// check for validity on the server side
					err_obj = { "variable": "email_match", "error": NULL_EMAIL_MATCH_ERROR, "message": INVALID_EMAIL_MESSAGE };
					errors = regex_match(email, EMAIL_REGEX, errors, err_obj);
					err_obj = null;

					err_obj = { "variable": "password_match", "error": NULL_PASSWORD_MATCH_ERROR, "message": INVALID_PASSWORD_MESSAGE };
					errors = regex_match(psswd, PSSWD_REGEX, errors, err_obj);
					err_obj = null;

					if (errors.length != 0) {
						// This is because we have matching errors
						// run error posting
						return display_registration_errors(errors);
					}


					// make sure the confirmation password matches
					if (confirm_psswd != psswd) {
						errors.push({
							"variable": "confirm_match",
							"error": NULL_CONF_PASSWROD_MATCH_ERROR,
							"message": INVALID_CONFIRM_PASSWORD_MESSAGE
						});
					}

					// console.log(errors)
					if (errors.length != 0) {
						// This is because we have matching errors
						// run error posting
						return display_registration_errors(errors);
					}

					// Old compatibility code, no longer needed.
					if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
					    httpRequest = new XMLHttpRequest();
					} else if (window.ActiveXObject) { // IE 6 and older
					    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
					}

					
					httpRequest.open("PUT", BASE_URL + ":" + BASE_PORT + "users/create");
					httpRequest.setRequestHeader('Content-Type', 'application/json');

					httpRequest.onload = function() {
						if ( httpRequest.status === 200 ) {
							console.log(httpRequest.responseText);
							successful_user_creation(email);
						} else if (httpRequest.status !== 200) {
							console.log(httpRequest.responseText);
						}
					};

					httpRequest.send(JSON.stringify({ "email": email, "password" : psswd }));

				}
			}
		},
		{
			view:"button", value:"Cancel",
			on: {
				onItemClick: (id, e) => {
					go_to_login();
				}
			}
		}
	]
}

webix.ui({
	view: "multiview",
	id: "center_view_multi",
	gravity: 5,
	cells: [
		{
			id:"sign_in_view",
			rows:[
				{},
				{
					height: SCREEN_RES_HEIGHT,
					cols: [
						{ width: SCREEN_RES_WIDTH },
						login_component,
						{}
					]
				},
				{}
			]
		},
		{
			id:"registration_view",
			rows:[
				{},
				{
					height: SCREEN_RES_HEIGHT,
					cols: [
						{ width: SCREEN_RES_WIDTH },
						register_component,
						{}
					]
				},
				{}
			]
		}
	]
});

console.log(screen.width)

$$("center_view_multi").setValue("sign_in_view");