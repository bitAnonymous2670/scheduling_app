const pg = require("pg");

const config = require(BASE + "/config.json");

const db_config = {
	user: config.database.user,
	password: config.database.password,
	database: config.database.database,
	host: config.database.host,
	port: config.database.port,
	idleTimeoutMillis: 2000
};

const user_pool = {
	pool: new pg.Pool(db_config)
}

module.exports = {
	create_users: (user_email, user_password, user_salt, callback) => {
		user_pool.pool.connect((err, client, done) => {
			if (err) return callback(err, null);
			const sql_query = "SELECT * FROM scheduler.create_user($1,$2,$3);";
			const params = [user_email, user_password, user_salt];
			client.query(sql_query, params, (err, result) =>{
				if (err) {
					done();
					return callback(err, null);
				}

				let _return = result.rows[0];

				let missing_properties = [];

				if (!_return.hasOwnProperty("success"))
					missing_properties.push("Missing Property \"success\"");

				if (!_return.hasOwnProperty("error_int"))
					missing_properties.push("Missing Property \"error_int\"");

				if (!_return.hasOwnProperty("err_message"))
					missing_properties.push("Missing Property \"err_message\"");

				if (0 < missing_properties.length) {
					done();
					return callback({"error": "Missing Properties.", "missing_array": missing_properties});
				}

				if (_return.error_int < 0) {
					done();
					return callback(_return.err_message, null);
				}

				done();
				return callback(null, _return.success);
			});
		});
	}, get_user_salt: (user_email, callback) => {
		user_pool.pool.connect((err, client, done) => {
			if (err) return callback(err, null);
			const sql_query = "SELECT * FROM scheduler.get_user_salt($1);";
			const params = [user_email];
			client.query(sql_query, params, (err, result) =>{
				if (err) {
					done();
					return callback(err, null);
				}

				let _return = result.rows[0];

				let missing_properties = [];

				if (!_return.hasOwnProperty("success"))
					missing_properties.push("Missing Property \"success\"");

				if (!_return.hasOwnProperty("error_int"))
					missing_properties.push("Missing Property \"error_int\"");

				if (!_return.hasOwnProperty("err_message"))
					missing_properties.push("Missing Property \"err_message\"");

				if (0 < missing_properties.length) {
					done();
					return callback({"error": "Missing Properties.", "missing_array": missing_properties});
				}

				if (_return.error_int < 0) {
					done();
					return callback(_return.err_message, null);
				}

				done();
				return callback(null, _return.success);
			});
		});
	}, validate_user_email: (user_email, password_hash, callback) => {
		user_pool.pool.connect((err, client, done) => {
			if (err) return callback(err, null);
			const sql_query = "SELECT * FROM scheduler.validate_user($1, $2);";
			const params = [user_email, password_hash];
			client.query(sql_query, params, (err, result) =>{
				if (err) {
					done();
					return callback(err, null);
				}

				let _return = result.rows[0];

				let missing_properties = [];

				if (!_return.hasOwnProperty("success"))
					missing_properties.push("Missing Property \"success\"");

				if (!_return.hasOwnProperty("error_int"))
					missing_properties.push("Missing Property \"error_int\"");

				if (!_return.hasOwnProperty("err_message"))
					missing_properties.push("Missing Property \"err_message\"");

				if (0 < missing_properties.length) {
					done();
					return callback({"error": "Missing Properties.", "missing_array": missing_properties});
				}

				if (_return.error_int < 0) {
					done();
					return callback(_return.err_message, null);
				}

				done();
				return callback(null, _return.success);
			});
		});
	}
}