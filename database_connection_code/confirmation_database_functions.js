const pg = require("pg");

const config = require(BASE + "/config.json");

const db_config = {
	user: config.database.user,
	password: config.database.password,
	database: config.database.database,
	host: config.database.host,
	port: config.database.port,
	idleTimeoutMillis: 2000
};

const confirmation_pool = {
	pool: new pg.Pool(db_config)
}

module.exports = {
	confirm_users: (confirmation_code, callback) => {
		confirmation_pool.pool.connect((err, client, done) => {
			if (err) return callback(err, null);
			const sql_query = "SELECT * FROM scheduler.confirm_user($1);";
			const params = [confirmation_code];
			client.query(sql_query, params, (err, result) =>{
				if (err) {
					done();
					return callback(err, null);
				}

				let _return = result.rows[0];

				let missing_properties = [];

				if (!_return.hasOwnProperty("success"))
					missing_properties.push("Missing Property \"success\"");

				if (!_return.hasOwnProperty("error_int"))
					missing_properties.push("Missing Property \"error_int\"");

				if (!_return.hasOwnProperty("err_message"))
					missing_properties.push("Missing Property \"err_message\"");

				if (0 < missing_properties.length) {
					done();
					callback({"error": "Missing Properties.", "missing_array": missing_properties});
				}

				if (_return.error_int < 0) {
					done();
					return callback(_return.err_message, null);
				}

				done();
				return callback(null, _return.success);
			});
		});
	}
}