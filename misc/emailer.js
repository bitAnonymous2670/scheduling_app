const nodemailer = require("nodemailer");


// async..await is not allowed in global scope, must use a wrapper
async function send_mail(to, confirmation_value) {
	// Generate test SMTP service account from ethereal.email
	// Only needed if you don't have a real mail account for testing
	let testAccount = await nodemailer.createTestAccount();

	// create reusable transporter object using the default SMTP transport
	let transporter = nodemailer.createTransport({
		host: "127.0.0.1",
		port: 1025,
		secure: false, // true for 465, false for other ports
		auth: { user: " ", pass: " " }
	});

	// send mail with defined transport object
	let info = await transporter.sendMail({
		from: '"Scheduler Confirmation" <donotreply@learner-for-hire.com>', // sender address
		to: to, // list of receivers
		subject: "Welcome To Scheduler!", // Subject line
		text: "Your Confirmation Number Is: " + confirmation_value, // plain text body
		html: "<a href=\"https://192.168.1.202:3000/confirm?value=" + confirmation_value + "\
				\">Click Here </a> To Confirm Your Account."
	});

	console.log("Message sent: %s", info.messageId);
	// Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

	// Preview only available when sending through an Ethereal account
	console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
	// Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

module.exports = send_mail;