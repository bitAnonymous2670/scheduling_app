const crypto = require("crypto");
const fs = require("fs");
const config = require(BASE + "/config.json");

const private = { 
	generate_salt: (byte_num, cb) => {
		if (!byte_num) byte_num = config.api_salt_length;
		crypto.randomBytes(byte_num, (err, buf) => {
			return cb(buf.toString("hex"));
		})
	}, hash_password: (passwd, salt, cb) => {
		return cb({
			"hash_password": crypto.createHmac("sha256", salt).update(passwd).digest("hex"),
			"salt": salt
		});
	}
};


module.exports = {
	generate_salt: private.generate_salt,
	hash_password: private.hash_password,
	session_id: (usr_name, passwd, secret, cb) => {
		return cb(crypto.createHmac("sha256", usr_name).update(passwd).update(secret).digest("hex"))
	} 
}