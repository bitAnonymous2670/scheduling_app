
import json, os

def read_config_file(filename):
	data = None

	if not os.path.exists(filename):
		return None

	with open(filename) as f:
		try:
			data = json.loads(f.read())
		except Exception as e:
			print(e)
			return None

	return data
