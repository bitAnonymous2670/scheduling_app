CREATE TYPE scheduler.create_user_result AS (
	success 	TEXT,
	error_int   INTEGER,
	err_message TEXT
);