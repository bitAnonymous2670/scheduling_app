CREATE TABLE IF NOT EXISTS scheduler.user_confirmation_table (
	table_id SERIAL PRIMARY KEY,
	user_id BIGINT,
	confirmation_value TEXT,
	insert_datetime INTEGER DEFAULT round( date_part( 'epoch', now() ) )
)