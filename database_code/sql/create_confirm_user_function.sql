CREATE FUNCTION confirm_user(in_confirmation_code TEXT)
	RETURNS scheduler.confirm_user_result AS $$
	DECLARE out_user_id INTEGER;
	DECLARE out_user_email TEXT;
	DECLARE confirmation_count INTEGER;
	DECLARE _return scheduler.confirm_user_result;
BEGIN

	out_user_email = NULL;

	SELECT user_id INTO out_user_id FROM scheduler.user_confirmation_table
	WHERE confirmation_value = in_confirmation_code;

	SELECT user_email INTO out_user_email FROM scheduler.scheduler_users
	WHERE table_id = out_user_id;

	IF out_user_email IS NULL THEN 
		_return.success = '';
		_return.error_int = -1;
		_return.err_message = 'Invalid User Confirmation Code.';
		RETURN _return;
	END IF;

	UPDATE scheduler.scheduler_users
	SET user_confirmed = TRUE WHERE table_id = out_user_id;

	IF NOT FOUND THEN 
		_return.success = '';
		_return.error_int = -2;
		_return.err_message = 'Unable To Update User. Reason Unknown';
		RETURN _return;
	END IF;

	_return.success = out_user_email;
	_return.error_int = 0;
	_return.err_message = '';
	RETURN _return;
END;
$$
LANGUAGE plpgsql;