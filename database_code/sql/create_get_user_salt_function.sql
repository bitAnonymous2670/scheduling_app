CREATE FUNCTION get_user_salt(in_user_salt TEXT) 
	RETURNS scheduler.confirm_user_result AS $$ 

	DECLARE out_user_salt TEXT;
	DECLARE _return scheduler.confirm_user_result;
BEGIN

	out_user_salt = NULL;

	SELECT user_salt INTO out_user_salt FROM scheduler.scheduler_users
	WHERE user_email = in_user_salt;

	IF out_user_salt IS NULL THEN
		_return.success = '';
		_return.error_int = -1;
		_return.err_message = 'Invalid User Credentials.';
		RETURN _return;
	END IF;

	_return.success = out_user_salt;
	_return.error_int = 0;
	_return.err_message = '';
	RETURN _return;

END;
$$
LANGUAGE plpgsql;