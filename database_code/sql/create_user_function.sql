CREATE FUNCTION create_user(in_username TEXT, in_password TEXT, in_salt TEXT)
	RETURNS scheduler.create_user_result AS $$
	DECLARE out_user_id INTEGER;
	DECLARE out_confirmation TEXT;
	DECLARE confirmation_count INTEGER;
	DECLARE _return scheduler.create_user_result;
BEGIN
	
	confirmation_count = 1;

	LOOP 

		SELECT scheduler.random_text_simple(10) INTO out_confirmation;

		SELECT COUNT(*) INTO confirmation_count FROM scheduler.user_confirmation_table uct
		WHERE uct.confirmation_value = out_confirmation;

		EXIT WHEN confirmation_count = 0;
	END LOOP;

	INSERT INTO scheduler.scheduler_users(user_email,user_password_hash,user_salt)
	VALUES (in_username,in_password,in_salt)
	RETURNING table_id INTO out_user_id;

	IF out_user_id < 0 THEN
		_return.success = '';
		_return.error_int = -1;
		_return.err_message = 'Unable To Create New User.';
		RETURN _return;
	END IF;

	INSERT INTO scheduler.user_confirmation_table(user_id, confirmation_value)
	VALUES (out_user_id, out_confirmation)
	RETURNING table_id INTO confirmation_count;

	IF confirmation_count < 0 THEN 
		_return.success = '';
		_return.error_int = -2;
		_return.err_message = 'Unable To Create New Confirmation.';
		RETURN _return;
	END IF;

	_return.success = out_confirmation;
	_return.error_int = 0;
	_return.err_message = '';
	RETURN _return;
END;
$$
LANGUAGE plpgsql;