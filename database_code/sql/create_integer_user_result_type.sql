CREATE TYPE scheduler.integer_user_result AS (
	success		INTEGER,
	error_int	INTEGER,
	err_message TEXT
);