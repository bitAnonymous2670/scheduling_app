CREATE TABLE IF NOT EXISTS scheduler.scheduler_users (
	table_id SERIAL PRIMARY KEY,
	user_email TEXT,
	user_password_hash TEXT,
	user_salt TEXT,
	user_confirmed BOOLEAN DEFAULT FALSE,
	insert_datetime INTEGER DEFAULT round( date_part( 'epoch', now() ) )
)