-- TODO: log all login attempts

CREATE FUNCTION validate_user(in_user_email TEXT, in_password_hash TEXT)
	RETURNS scheduler.integer_user_result AS $$

	DECLARE out_user_id INTEGER;
	DECLARE _return scheduler.integer_user_result;

BEGIN

	out_user_id = NULL;

	SELECT su.table_id INTO out_user_id FROM scheduler.scheduler_users su
	WHERE su.user_email LIKE in_user_email AND su.user_password_hash LIKE in_password_hash AND su.user_confirmed;

	IF out_user_id IS NULL THEN
		_return.success = -1;
		_return.error_int = -1;
		_return.err_message = 'Invalid User Credentials.';
		RETURN _return;
	END IF;

	_return.success = out_user_id;
	_return.error_int = 0;
	_return.err_message = '';
	RETURN _return;

END;
$$
LANGUAGE plpgsql;