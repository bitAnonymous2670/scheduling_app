import os, json, logging
import psycopg2
import read_config


CONFIG = None
CONN_STR = None
CONN = None
LOG_FILE = None
LOGGER = None

CONN_STR_FRMT = "dbname={} user={} password={} host={} port={}"
POST_STR_FRMT = "dbname={} user={} host={} port={}"
CONFIG_REQS = [
	"host", "user", "port",
	"database", "password"
]

LOGGING_ERROR = False

CONF_FILE = os.path.dirname(os.path.realpath(__file__)) + os.sep + "config.json"


if not os.path.exists(CONF_FILE):
	exit("The config file does not exist.")


if __name__ == "__main__":
	CONFIG = read_config.read_config_file(CONF_FILE)

if __name__ == "__main__":
	LOG_FILE = os.path.dirname(os.path.realpath(__file__)) + os.sep + CONFIG["logging"]["file_name"]
	logging.basicConfig(filename=LOG_FILE,  format=CONFIG["logging"]["format"], filemode='w') 
	LOGGER = logging.getLogger()


def set_config(conf):
	global CONFIG, CONFIG_REQS, LOGGER, LOGGING_ERROR

	if "database" not in conf:
		LOGGER.error("Keyword \"database\" is not in the conf file")
		LOGGING_ERROR = True
		return -1

	for val in CONFIG_REQS:
		if val not in conf["database"]:
			return -1

	CONFIG = conf

	return 0


def set_conn_str():
	global CONFIG, CONN_STR, LOGGER, LOGGING_ERROR

	if CONFIG is None:
		LOGGER.error("Config File Has Not Been Initialized Yet.")
		LOGGING_ERROR = True
		return -1

	CONN_STR = CONN_STR_FRMT.format(CONFIG["database"]["database"], CONFIG["database"]["user"], 
									CONFIG["database"]["password"], CONFIG["database"]["host"], 
									CONFIG["database"]["port"])

	return 0


def connect():
	global CONN, CONN_STR, LOGGER, LOGGING_ERROR

	if CONN_STR is None:
		LOGGER.error("Connection String Has Not Been Initialized Yet.")
		LOGGING_ERROR = True
		return -1

	try:
		CONN = psycopg2.connect(CONN_STR)
		CONN.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
	except Exception as e:
		LOGGER.error("Unable To Connect To Postgres Server.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1
	
	try:
		curs = CONN.cursor()
	except Exception as e:
		LOGGER.error("Unable To Connect To Create A Cursor To Postres Server.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1

	try:
		curs.execute("SELECT 1;")
		curs.close()
		return 0
	except Exception as e:
		LOGGER.error("Connection Test To Postgres Server Didnt Work.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1

def close_connection():
	global CONN, LOGGER, LOGGING_ERROR

	try:
		CONN.close()
	except Exception as e:
		LOGGER.error("I dont know what to do here.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1

	return 0


def initial_db_script(dict_key):

	global CONFIG, POST_STR_FRMT, LOGGER, LOGGING_ERROR

	conn_str = POST_STR_FRMT.format("postgres", "postgres", 
									CONFIG["database"]["host"], CONFIG["database"]["port"])

	try:
		conn = psycopg2.connect(conn_str)
		conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
	except Exception as e:
		LOGGER.error("Unable To Connect To Postgres Server.")
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	try:
		f = open(CONFIG["files"][dict_key])
	except Exception as e:
		LOGGER.error("Unable To Read Config File Key: {}".format(dict_key))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	sql_str = f.read()
	f.close()

	try:
		curs = conn.cursor()
		curs.execute(sql_str)
		curs.close()
	except Exception as e:
		LOGGER.error("Unable to Run SQL from File: {}".format(CONFIG["files"][dict_key]))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	conn.close()

	return 0


def create_db():
	return initial_db_script("create_database")

def drop_db():
	return initial_db_script("drop_database")


def drop_user():
	return initial_db_script("drop_user")


def create_user_set_password():

	if initial_db_script("create_user") < 0:
		return -1

	if initial_db_script("set_password") < 0:
		return -1

	return 0


def grant_permissions():
	return initial_db_script("scheduler_permissions")


def create_schema():
	global CONN, CONFIG, LOGGER, LOGGING_ERROR

	curs = None

	if CONN is None:
		LOGGER.error("Connection Has Not Been Initialized Yet.")
		LOGGING_ERROR = True
		return -1

	try:
		curs = CONN.cursor()
	except Exception as e:
		LOGGER.error("Unable To Connect To Create A Cursor To Postres Server.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1

	schema_file = "create_schema"
	try:
		f = open(CONFIG["files"][schema_file])
	except Exception as e:
		LOGGER.error("Unable To Read Config File Key: {}".format(schema_file))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	sql_str = f.read()
	f.close()

	try:
		curs = CONN.cursor()
		curs.execute(sql_str)
		curs.close()
	except Exception as e:
		LOGGER.error("Unable to Run SQL from File: {}".format(CONFIG["files"][schema_file]))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	return 0


def create_table(table_file):
	global CONFIG, LOGGER, LOGGING_ERROR, CONN

	try:
		f = open(CONFIG["files"][table_file])
	except Exception as e:
		LOGGER.error("Unable To Read Config File Key: {}".format(table_file))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	sql_str = f.read()
	f.close()

	try:
		curs = CONN.cursor()
		curs.execute(sql_str)
		curs.close()
	except Exception as e:
		LOGGER.error("Unable to Run SQL from File: {}".format(CONFIG["files"][table_file]))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1
	return 0



def create_tables():
	global CONN, CONFIG, LOGGER, LOGGING_ERROR

	curs = None

	if CONN is None:
		LOGGER.error("Connection Has Not Been Initialized Yet.")
		LOGGING_ERROR = True
		return -1

	try:
		curs = CONN.cursor()
	except Exception as e:
		LOGGER.error("Unable To Connect To Create A Cursor To Postres Server.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1

### Create User Table
	if create_table("create_user_table") < 0: return -1
### Create User Confirmation Table
	if create_table("create_user_confirmation_table") < 0: return -1

	return 0


def general_sql_create(dict_key):
	global CONN, CONFIG, LOGGER, LOGGING_ERROR

	curs = None

	if CONN is None:
		LOGGER.error("Connection Has Not Been Initialized Yet.")
		LOGGING_ERROR = True
		return -1

	try:
		curs = CONN.cursor()
	except Exception as e:
		LOGGER.error("Unable To Connect To Create A Cursor To Postres Server.")
		LOGGING_ERROR = True
		LOGGER.error(e)
		return -1


	sql_file = dict_key
	try:
		f = open(CONFIG["files"][sql_file])
	except Exception as e:
		LOGGER.error("Unable To Read Config File Key: {}".format(sql_file))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1

	sql_str = f.read()
	f.close()

	try:
		curs = CONN.cursor()
		curs.execute(sql_str)
		curs.close()
	except Exception as e:
		LOGGER.error("Unable to Run SQL from File: {}".format(CONFIG["files"][sql_file]))
		LOGGER.error(e)
		LOGGING_ERROR = True
		return -1
	return 0


def create_functions():

	if general_sql_create("create_user_function") < 0:
		exit("Unable To Run create_user_function Code")

	if general_sql_create("create_random_string_function") < 0:
		exit("Unable To Run create_random_string_function Code")

	if general_sql_create("create_random_range_function") < 0:
		exit("Unable To Run create_random_range_function Code")

	if general_sql_create("create_user_confirmation_function") < 0:
		exit("Unable To Run create_user_confirmation_function Code")

	if general_sql_create("create_get_user_salt_function") < 0:
		exit("Unable To Run create_get_user_salt_function Code")

	if general_sql_create("validate_user") < 0:
		exit("Unable To Run validate_user Code")

	return 0


def create_types():
	if general_sql_create("new_user_confirm_return_type") < 0:
		exit("Unable To Run new_user_confirm_return_type Code")


	if general_sql_create("confirm_user_return_type") < 0:
		exit("Unable To Run confirm_user_return_type Code")

	if general_sql_create("integer_user_result") < 0:
		exit("Unable To Run integer_user_result Code")

	return 0


if __name__ == "__main__":

	if drop_db() < 0:
		exit("Could Not Run drop_database Code.")

	if drop_user() < 0:
		exit("Could Not Run drop_user Code")


## Initialize User
	if create_user_set_password() < 0:
		exit("Could Not Run create_user_set_password Code")


## Initialize Database
	if create_db() < 0:
		exit("Could Not Run create_database Code.")


## Grant User DB Permissions
	if grant_permissions() < 0:
		exit("Could Not Run grant_permissions Code.")


## Initialize Permanent Connection
	if set_conn_str() < 0:
		exit("Could Not Initialize DB CONN String: set_conn_str()")

	if connect() < 0:
		exit("Could Not Run connect() Code")


## Create Schema
	if create_schema() < 0:
		exit("Could Not Run create_schema() Code")


## Create Tables
	if create_tables() < 0:
		exit("Could Not Create All Tables.")


## Create Types
	if create_types() < 0:
		exit("Could Not Create All Types.")


## Create Functions
	if create_functions() < 0:
		exit("Could Not Create All Functions.")






	close_connection()




