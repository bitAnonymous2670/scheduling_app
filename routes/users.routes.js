const express = require('express');
const router = express.Router();

const hashing_functions = require(BASE + "/misc/hashing_functions.js");
const user_db_conn = require(BASE + "/database_connection_code/user_database_functions.js");
const emailer = require(BASE + "/misc/emailer.js");

router.put("/create", (req, res) => {

	if (!req.body.email) return res.status(400).json({"err": "Did not send email"});
	if (!req.body.password) return res.status(400).json({"err": "Did not send password"});

	hashing_functions.generate_salt(null, (salt) => {
		hashing_functions.hash_password(req.body.password, salt, (user_info) => {
			user_db_conn.create_users(req.body.email, user_info.hash_password, salt, (err, result) => {

				if (err) return res.status(400).json({"err": err});

				emailer(req.body.email, result);

				res.status(200).json({"result": result});
			});
		});
	});
});

module.exports = router;