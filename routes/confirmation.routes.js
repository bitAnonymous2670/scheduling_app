const express = require('express');
const router = express.Router();

const confirmation_db_conn = require(BASE + "/database_connection_code/confirmation_database_functions.js");

router.get("/", (req, res) => {

	if (!req.query.value) return res.status(400).json({"err": "No Confirmation Value Was Sent."});

	confirmation_db_conn.confirm_users(req.query.value, (err, result) => {
		if (err) return res.status(400).json({"err": err});

		res.status(200).json("Congrats, " + result + ", Your Have Been Confirmed!");
	});

});

module.exports = router;