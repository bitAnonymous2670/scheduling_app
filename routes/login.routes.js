/*
	
	TODO: Documentation
	TODO: Fill Out Conditions
	TODO: Update Validate User DB To Check If The User Has Been Confirmed Yet

*/

const express           = require('express');
const router            = express.Router();

const jwt               = require("jsonwebtoken");

const hashing_functions = require(BASE + "/misc/hashing_functions.js");
const user_db_conn      = require(BASE + "/database_connection_code/user_database_functions.js");

const config            = require(BASE + "/config.json");


/*
	
//
		URL: /users/authorize
		VERB: PUT
		Passed Requirements:
			1. user_name
			2. user_password
		Purpose:
			Creates a token and a session_id for passed user. The token and session_id
			are used to validate a users actions based on roles/permissions and the 
			session_id is used to keep track of when a user did something during a 
			particular logged-in session.
		
		Returned:
			{
				"token": ...,
				"session_id": ... 
			}
//


router.put('/authorize', function(req, res, next) {
	// Look up correct error codes is something is not passed back 
	// comment the procedure of this

	if (!req.body.user_name) return res.status(400).json({"err": "Did not send user name"});
	if (!req.body.user_password) return res.status(400).json({"err": "Did not send password"});

	const user_name = req.body.user_name;
	const user_password = req.body.user_password;

	user_conn.get_user_salt(user_name, (err, salt) => {

		if (err) {
			if (err.message == "User with user_name, " + user_name + ", does not exist.")
				return res.status(400).json({"err": "Unable to match Username."});
			else
				return res.status(400).json({"err": err});
		}

		user_auth.hash_password(user_password, salt, (user_info) => {
			const hash_psswd = user_info.hash_password;
			user_conn.validate_user(user_name, hash_psswd, (err, result) => {
				if (err && err.message == "User cannot be validated.") {
					auth_conn.log_auth(null, user_name, {
						"err": "Cannot Find user_name"
					}, (err, result) => {
						if (err) console.log(err);
					});

					// only because we dont want to give them too much info
					return res.status(401).json({"err": "Invalid Login Credentials"});
				}

				if (err &&  err.message == "Users credentials are not valid.") {

					auth_conn.log_auth(-1, user_name, {
						"err": "Invalid Login Credentials"
					}, (err, result) => {
						if (err) console.log(err);
					});

					return res.status(401).json({"err": "Invalid Login Credentials"});
				}

				if (err) return res.status(400).json({"err": err});

				const valid_user_id = result;
			const token = jwt.sign({ "user_name": user_name }, config.api_secret, { expiresIn: '1h' });

				user_auth.session_id(user_name, hash_psswd, config.api_secret, (session_id) => {
					res.status(200).json({"result": {"token": token, "session_id":session_id}});

					auth_conn.log_auth(valid_user_id, user_name, {
						"token": token, "session_id": session_id
					}, (err, result) => {
						if (err) console.log(err);
					});
				});
			});
		});
	});
});

*/

router.post("/", (req, res) => {

	if (!req.body.email) return res.status(400).json({"err": "Did not send email"});
	if (!req.body.password) return res.status(400).json({"err": "Did not send password"});

	if (!req.cookies && !req.cookies.sesson_id) return res.status(400).json({"err": "Unknown Error."});

	let email = req.body.email;

	user_db_conn.get_user_salt(email, (err, salt) => {
		if (err) if (err) return res.status(400).json({"err": err});
		
		hashing_functions.hash_password(req.body.password, salt, (user_info) => {
			const hash_psswd = user_info.hash_password;

			user_db_conn.validate_user_email(email, hash_psswd, (err, result) => {

				if (err) return res.status(400).json({"err": err});

				const valid_user_id = result;
				const token = jwt.sign({ "email": email }, config.api_secret, { expiresIn: '1h' });

				res.cookie("token", token)
				.cookie("session_id", req.cookies.session_id)
					.status(200)
					.json({"result": 1});

				// res.status(200).json({"result": {"token": token }});
			});

		});

	});
});

module.exports = router;