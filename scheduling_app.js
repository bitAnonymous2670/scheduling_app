// TODO: Read up on what to do with environment variables
// TODO: Logging

const sprintf = require('sprintf-js').sprintf

if (!process.env.SERVER) {
	console.log("%s does not exist under the environment variables.", "SERVER");
	process.exit();
}

const server = process.env.SERVER;

if (!process.env.SCHEDULER_PORT) {
	console.log("%s does not exist under the environment variables.", "SCHEDULER_BASE");
	process.exit();
}

global.BASE = process.env.SCHEDULER_BASE;


const fs = require("fs");
const https = require("https");

const uuid = require('uuid/v4');

const express = require("express");
const app = express();

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');


// Vars
const config = require(BASE + "/config.json");


// TODO: Make These env varirables
// Globals
const PORT = config.port;

const user_routes = require(BASE + "/routes/users.routes.js");
const confirmation_routes = require(BASE + "/routes/confirmation.routes.js");
const login_routes = require(BASE + "/routes/login.routes.js");


app.use(express.static('public'));

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/users', user_routes);
app.use('/confirm', confirmation_routes);
app.use('/login', login_routes);


// add & configure middleware
app.use(session({
	genid: (req) => { return uuid(); }, // use UUIDs for session IDs
	secret: 'keyboard cat', resave: false, saveUninitialized: true
}));


var visits = 0;

app.get("/", function(req, res) {
	res.sendFile(BASE + "/public/scheduler.html");
});



https.createServer({
	key: fs.readFileSync("certs/server.key"),
	cert: fs.readFileSync("certs/server.crt")
}, app).listen(PORT)


